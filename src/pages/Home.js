import React from 'react'
import { ProjectPanel, Prototype } from '../components'
const Home = () => {
  return (

    <div className="e-wrapper l-home">
      <section id="about" className="e-section c-about e-section--white">
        <div className="e-section__content">
          <h1>Curabitur aliquet quam id dui posuere blandit. Cras ultricies ligula sed magna dictum porta. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Cras ultricies ligula sed magna dictum porta. Pellentesque in ipsum id orci porta dapibus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque in ipsum id orci porta dapibus. Pellentesque in ipsum id orci porta dapibus. Pellentesque in ipsum id orci porta dapibus.</h1>
        </div>
      </section>
      <section id="projects" className="e-section c-projects e-section--marigold">
        <div className="e-section__content">
          <div className="c-projects__wrapper">
            <ProjectPanel />
            <ProjectPanel />
            <ProjectPanel />
          </div>
        </div>
      </section>
      <section id="prototype" className="e-section c-practice e-section--rose">
        <div className="e-section__content">
          <h2>Practice shots</h2>
          <p>I practice in small 'project', often after following tutorials and other projects to go over key concepts. I sometimes document my process. </p>
          <ul className="c-practice__wrapper">
            <Prototype />
            <Prototype />
            <Prototype />
            <Prototype />
          </ul>
        </div>
      </section>
    </div>
  )
}

export default Home