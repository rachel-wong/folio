import React from 'react'
import { Link } from 'react-router-dom'

const Footer = () => {
  return (
    <>
    <footer className="e-section e-section--navy">
      <div className="e-section__content">
          <h3>I can be found elsewhere, including: </h3>
          <ul className="c-footer__links">
            <li className="c-footer__links__item">
              <a href="http://www.google.com">
                <span>Bitbucket</span>
              </a>
            </li>
            <li className="c-footer__links__item">
              <a href="http://www.google.com">Codepen</a>
            </li>
            <li className="c-footer__links__item">
              <a href="http://www.google.com">Github</a>
            </li>
            <li className="c-footer__links__item">
              <a href="http://www.google.com">Email</a>
            </li>
          </ul>
          <small className="f-footer__credit">
            <span>Made with &copy; <a href="https://sanity.io/">Sanity.io</a>, <a href="https://reactjs.org/">ReactJS</a> and <a href="https://netlify.com/">Netlify</a>.</span>
            <span>Back to top</span>
          </small>
      </div>
    </footer>
      </>
  )
}

export default Footer