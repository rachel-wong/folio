import React from 'react'
import { HiCake } from "react-icons/hi";
import { IconContext } from "react-icons";

const Prototype = () => {
  return (
    <li className="c-prototype">
      <IconContext.Provider value={{ color: "white", className: "c-prototype__icon"}}>
        <HiCake />
      </IconContext.Provider>
      <div className="c-prototype__content">
        <h3 className="c-prototype__heading">Some title goes here over two lines</h3>
        <p className="c-prototype__summary">Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Nulla porttitor accumsan tincidunt. Cras ultricies ligula sed magna dictum porta.</p>
        <div className="c-prototype__actions">
          <a href="http://www.github.com" className="c-prototype__link c-prototype__link--repo">Codebase</a>
          <a href="http://www.google.com" className=" c-prototype__link c-prototype__link--live">See Prototype</a>
        </div>
      </div>
    </li>
  )
}

export default Prototype