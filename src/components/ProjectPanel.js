import React from 'react'
import SampleImage from '../assets/sample.png'

const ProjectPanel = () => {
  return (
    <div className="c-panel">
      <h4 className="c-panel__heading">Project panel 1</h4>
      <div className="c-panel__imagewrapper">
        <img src={ SampleImage } alt="sample" />
      </div>
      <ul className="c-panel__tags">
        <li className="c-panel__tags__item"><span className="c-panel__tags__item__label">HTML5</span></li>
        <li className="c-panel__tags__item"><span className="c-panel__tags__item__label">CSS3</span></li>
        <li className="c-panel__tags__item"><span className="c-panel__tags__item__label">Javascript</span></li>
      </ul>
      <div className="c-panel__summary">
        <p>Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Donec rutrum congue leo eget malesuada. Nulla quis lorem ut libero malesuada feugiat. Nulla porttitor accumsan tincidunt. Nulla porttitor accumsan tincidunt. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae.</p></div>
      <div className="c-panel__actions">
        <a href="https://www.google.com" className="e-button c-panel__btn">See it Live</a>
        <a href="https://www.github.com" className="e-button c-panel__btn">Codebase</a>
      </div>
    </div>
  )
}

export default ProjectPanel