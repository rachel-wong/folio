import React from 'react'
import { NavLink, Link } from 'react-router-dom'
import AnchorLink from 'react-anchor-link-smooth-scroll'

const NavBar = () => {
  return (
    <section className="h-nav__wrapper">
      <div className="h-nav__content">
        <Link to={"/"} className="h-nav__logo-link"><h1 className="h-nav__logo">Folio Home</h1></Link>
        {/* // TODO do mobile nav and icons */}
        <button type="button" className="sr-only" data-nav-trigger>Toggle Mobile Nav insert icons here</button>
        <nav className="h-nav" data-nav>
          <NavLink className="h-nav__item" to="#about" activeClassName="h-nav__item--active">About</NavLink>
          <AnchorLink className="h-nav__item" href="#projects">Projects</AnchorLink>
          {/* <NavLink className="h-nav__item" to="#projects" activeClassName="h-nav__item--active">Projects</NavLink> */}
          <AnchorLink className="h-nav__item" href="#prototype">Practice</AnchorLink>
          {/* <NavLink className="h-nav__item" to="#practice" activeClassName="h-nav__item--active">Practice</NavLink> */}
          <NavLink className="h-nav__item" to="/blog" activeClassName="h-nav__item--active">Blog</NavLink>
        </nav>
        </div>
    </section>
  )
}

export default NavBar