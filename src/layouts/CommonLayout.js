import React from 'react'
import { NavBar } from '../components'
import { useLocation } from 'react-router-dom'

const CommonLayout = ({ children }) => {

  let { pathname } = useLocation()

  // different header colour depending on which route
  // very brittle - adding an extra page will require extending the switch statement cases
  let pageColour = ""

  switch (pathname) {
    case "/blog":
      pageColour = "e-section--marigold"
      break;
    case "/404":
      pageColour = "e-section--rose"
      break;
    default:
      break;
  }

  return (
    <div className={"l-layout__content--common " + pageColour}>
      <NavBar />
      { children }
    </div>
  )
}

export default CommonLayout