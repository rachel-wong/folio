import React from 'react'
import { NavBar } from '../components'
import SampleImage from '../assets/sample.png'

const HeroLayout = ({ children }) => {
  return (
    <div className="l-layout__content--hero">
      {/* insert background svgs in here */}
      <header className="c-header--hero e-section--green">
        <NavBar/>
        <div className="c-header--hero__content">
          <div className="c-header--hero__content__text">
            <h1>Hello! Heading text will go here</h1>
            <button type="button" className="e-button c-header--hero__content__button">See work</button>
          </div>
          <div className="c-header--hero__imagewrapper">
            <img src={SampleImage} alt="sample" />
          </div>
        </div>
        </header>
      <div className="c-header-wavewrap">
        <svg preserveAspectRatio="none" width="1440" height="74" viewBox="0 0 1440 74" className="c-header-wave">
        <path d="M456.464 0.0433865C277.158 -1.70575 0 50.0141 0 50.0141V74H1440V50.0141C1440 50.0141 1320.4 31.1925 1243.09 27.0276C1099.33 19.2816 1019.08 53.1981 875.138 50.0141C710.527 46.3727 621.108 1.64949 456.464 0.0433865Z"></path>
        </svg>
      </div>
      { children }
    </div>
  )
}

export default HeroLayout