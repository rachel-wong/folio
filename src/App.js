import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import { Home, Blog, BlogPost, NotFound, Project } from './pages/index'
import { Footer } from './components'
import { CommonLayout, HeroLayout } from './layouts'
import "./scripts/scripts"

import './scss/main.css'

// reusable function to parse different layout depending on props
const RouteWrapper = ({ component: Component, layout: Layout }) => {
  return (
    <Route render={(props) => (
      <Layout {...props}>
        <Component {...props} />
      </Layout>
    )} />
  )
}

function App() {
  return (
    <div className="App">
      <div className="l-page">
        <main className="l-page__content">
            <Router>
              <Switch>
                <RouteWrapper exact path="/" component={Home} layout={ HeroLayout } />
                <RouteWrapper path="/blog" component={Blog} layout={ CommonLayout } />
                <RouteWrapper path="/blog/:blogslug" component={ BlogPost } layout={ CommonLayout }/>
                <RouteWrapper path="/projects/:projectslug" component={ Project } layout={ CommonLayout } />
                <RouteWrapper path="/404" component={ NotFound } layout={ CommonLayout } />
                <Redirect to="*" component={ NotFound } />
              </Switch>
            </Router>
        </main>
        <Footer />
      </div>
    </div>
  );
}

export default App;
