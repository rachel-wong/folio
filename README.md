## Folio

**Dependencies**

- react-router-dom
- react-icons
- phosphor icons


## Todo

* select typefaces (space mono)
* json ld (schema.org)
* icons from the noun project could be considered [https://www.npmjs.com/package/the-noun-project](https://www.npmjs.com/package/the-noun-project)

### Learning points

* Different layouts for different routes can be implemented with the following

```javascript
// passed into App.js or wherever the Router and routes are declared
function RouteWrapper({component: Component, layout: Layout }) {
  return (
    <Route render={(props) => (
      <Layout {...props}>
        <Component {...props} />
      </Layout>
    )} />
  )
}

// App.js or wherever the Router and routes are declared.
<Router>
  <Switch>
    <RouteWrapper exact path="/" component={Home} layout={ HeroLayout } />
  </Switch>
</Router>
```

* Cannot style the `Gr` suite of icons from `react-icon` as at time of writing
